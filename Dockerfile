FROM debian:latest
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
WORKDIR /var/www/html
ENTRYPOINT ["/entrypoint.sh"]
EXPOSE 80
CMD ["apache2ctl", "-D", "FOREGROUND"]