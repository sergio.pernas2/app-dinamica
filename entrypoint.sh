#!/bin/bash

# set -e

# Evaluar si el comando ping existe, de lo contrario se instala.


apt update && apt install apache2 git -y

deploy_app () {

	cd /web-app && git pull
	cp -r /web-app/album/* /var/www/html
}


# Si el directorio '/web-app' no existe
# clonarlo

if ! test -d /web-app; then

	git clone https://gitlab.com/sergio.pernas2/app-dinamica.git /web-app 

	deploy_app
else

	# Si el repositorio ya se ha cloando
	# traer posibles cambios de la rama main

	deploy_app
fi





exec "$@"